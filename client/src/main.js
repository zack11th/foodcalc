import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.config.productionTip = false;

Vue.directive('scroll', {
  inserted: function (el, binding) {
    window.addEventListener('scroll', binding.value);
  },
  unbind: function (el, binding) {
    window.removeEventListener('scroll', binding.value)
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
