import axios from 'axios';
import {urlAPI} from './server.config';

export default {
  namespaced: true,
  state: {
    proteins: 100, // белки на неделю
    fats: 200, // жиры на неделю
    carbo: 300, // углеводы на неделю
    power: 2100, // энергии на неделю (ккал)
    products: [],
    totalProducts: 250,
    productsPerPage: 12,
    notEat: [], // массив id продуктов, которые пользователь не ест
    oldNotEat: []
    },
  mutations: {
    setGeneral(state, res) {
      // state.proteins = res.data.proteins;
      // state.fats = res.data.fats;
      // state.carbo = res.data.carbo;
      // state.power = res.data.power;
      // state.products = res.data.products;
      state.totalProducts = res.data[0];
    },
    setProducts(state, res) {
      state.products = res.data;
    },
    notEatIt(state, id) {
      state.notEat.push(id);
    },
    eatIt(state, id) {
      let ind = state.notEat.indexOf(id);
      if(ind !== -1) {
        state.notEat.splice(ind, 1);
      }
    },
    setOldNotEat(state) {
      state.oldNotEat = state.notEat.slice(0);
    }
  },
  actions: {
    getGeneral({commit, state, dispatch}, person) {
      commit('setOldNotEat');
      let data = {
        ...person,
        notEat: state.notEat
      };
      console.log(data)
      // axios.post(`${urlAPI}/general`, data)
      axios.get(`${urlAPI}/totalProducts`)
        .then(res => {
          commit('setGeneral', res);
          dispatch('getProducts', 1)
        })
    },
    getProducts({commit, state}, page = 1) {
      axios.get(`${urlAPI}/products?_page=${page}&_limit=${state.productsPerPage}`)
        .then(res => {
          commit('setProducts', res);
        })
    },
    notEatIt({commit}, id) {
      commit('notEatIt', id);
    },
    eatIt({commit}, id) {
      commit('eatIt', id);
    }
  },
  getters: {
    proteins(state) {
      return state.proteins;
    },
    fats(state) {
      return state.fats;
    },
    carbo(state) {
      return state.carbo;
    },
    power(state) {
      return state.power;
    },
    products(state) {
      return state.products;
    },
    pagesProducts(state) {
      return Math.ceil(state.totalProducts / state.productsPerPage);
    },
    notEatId(state) {
      return state.notEat;
    },
    changesProduct(state) {
      return state.oldNotEat < state.notEat || state.oldNotEat > state.notEat
    }
  }
}