import axios from 'axios';
import {urlAPI} from './server.config';

export default {
  namespaced: true,
  state: {
    list: [],
    recipe: null,
    perPage: 12,
    nextPage: 1,
    totalPages: null,
    loading: false
  },
  mutations: {
    setRecipes(state, res) {
      state.list.push(...res.data);
      state.totalPages = Math.ceil(res.headers['x-total-count'] / state.perPage);
      state.nextPage++;
    },
    setOneRecipe(state, res) {
      state.recipe = res.data;
    },
    loading(state, flag) {
      state.loading = flag;
    }
  },
  actions: {
    getRecipes({commit, state}, notEat) {
      commit('loading', true);
      // axios.get(`${urlAPI}/recipes`, {params: {notEat}})
      axios.get(`${urlAPI}/recipes?_page=${state.nextPage}&_limit=${state.perPage}`)
        .then(res => {
          commit('setRecipes', res);
          commit('loading', false);
        });
    },
    getOneRecipe({commit}, id) {
      commit('loading', true);
      axios.get(`${urlAPI}/recipeID`)
        .then(res => {
          commit('setOneRecipe', res);
          commit('loading', false);
        })
    }
  },
  getters: {
    list(state) {
      return state.list;
    },
    loading(state) {
      return state.loading;
    },
    moreRecipes(state) {
      return state.nextPage <= state.totalPages;
    },
    recipe(state) {
      return state.recipe;
    }
  }
}