import Vue from 'vue'
import Vuex from 'vuex'
import products from './products'
import recipes from './recipes'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    products, recipes
  },
  strict: process.env.NODE_ENV !== 'production'
})
